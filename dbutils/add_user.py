# this script is running separatly from server just to put user into database
from config import DATABASE, DB_HOST, DB_USER
from credentials_storage import CredentialsStorage
from security import hash_password
from user import User
from os import getenv
from user_role import UserRole

if __name__ == '__main__':
    # INPUT CONFIG MANUALLY BEFORE USE
    config = {
        "DATABASE": DATABASE,
        "DB_HOST": getenv("DB_HOST"),
        "DB_USER": DB_USER,
        "DB_PASS": getenv("DB_PASS")
    }
    storage = CredentialsStorage(config)
    username = input("Enter username: ")
    if storage.is_user_exists(username):
        raise AssertionError("User with that username already exists")
    password = input("Enter password: ")
    hash_pass = hash_password(password)
    user_role = input(f"Enter one of the following roles: {UserRole.SENIOR_TRADER.name} or {UserRole.TRADER.name}: ")
    storage.insert_user(User(username, hash_pass, user_role))
    print(f"User {username} inserted successfully")
