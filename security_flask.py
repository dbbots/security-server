import json
import logging

from flask import Flask, request
from flask_cors import CORS

from credentials_storage import CredentialsStorage
from security import verify_password
from user import User

logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')

app = Flask(__name__)
CORS(app)
app.config.from_object('config')
storage = CredentialsStorage(app.config)


@app.route('/auth/login', methods=['POST'])
def login_request():
    request_data = request.get_json()
    if request_data is None:
        return "Bad request: json can't be parsed", 400

    raw_user_list = storage.get_user_by_username(request_data['username'])
    if len(raw_user_list) == 0 or len(raw_user_list[0]) == 0:
        response_object = {
            'status': 'fail',
            'message': 'User does not exist.'
        }
        return response_object, 404
    raw_user = raw_user_list[0]
    user_id = raw_user[0]
    db_user = User(raw_user[1], raw_user[2], raw_user[3])
    hash_pass = db_user.password
    if verify_password(request_data['password'], hash_pass):
        auth_token = db_user.encode_auth_token(user_id, app.config["SECRET_KEY"])
        if auth_token:
            response_object = {
                'status': 'success',
                'message': 'Successfully logged in.',
                'auth_token': auth_token.decode()
            }
            return json.dumps(response_object), 200
        return 'Ok', 200
    else:

        return 'Unauthorized', 401


@app.route('/auth/status', methods=['GET'])
def check_user_request():
    auth_header = request.headers.get('Authorization')
    if auth_header:
        auth_token = auth_header.split(" ")[1]
    else:
        return user_not_exists_response()
    if auth_token:
        user_id = User.decode_auth_token(auth_token, app.config["SECRET_KEY"])
        print(user_id)
        if not isinstance(user_id, str):
            raw_user_list = storage.get_user_by_id(user_id)
            if len(raw_user_list) == 0 or len(raw_user_list[0]) == 0:
                return user_not_exists_response()
            raw_user = raw_user_list[0]
            db_user = User(raw_user[1], raw_user[2], raw_user[3])
            response_object = {
                'status': 'success',
                'data': {
                    'username': db_user.username,
                    'role': db_user.user_role
                }
            }
            return response_object, 200
        response_object = {
            'status': 'fail',
            'message': user_id
        }
        return response_object, 401

def user_not_exists_response():
    response_object = {
        'status': 'fail',
        'message': 'User does not exist.'
    }
    return response_object, 404

def bootapp():
    app.run(port=app.config["FLASK_PORT"], threaded=True, host=app.config["FLASK_HOST"])


if __name__ == '__main__':
    bootapp()
