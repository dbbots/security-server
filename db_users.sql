CREATE DATABASE  IF NOT EXISTS `db_users`;
USE `db_users`;

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  user_id int NOT NULL AUTO_INCREMENT,
  user_name varchar(45) NOT NULL,
  user_password varchar(100) NOT NULL,
  user_role varchar(45) NOT NULL,
   PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;