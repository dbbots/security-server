from os import getenv

FLASK_PORT = "8001"
FLASK_HOST = "0.0.0.0"

DB_HOST = getenv("DB_HOST")
DATABASE = "db_users"
DB_USER = "root"
DB_PASS = getenv("DB_PASS")
SECRET_KEY = getenv("SECRET_KEY")
