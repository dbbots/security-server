import mysql.connector


class CredentialsStorage:
    def __init__(self, config):
        self.db = mysql.connector.connect(host=config['DB_HOST'],
                                          database=config['DATABASE'],
                                          user=config["DB_USER"],
                                          password=config["DB_PASS"])
        self.usersCursor = self.db.cursor(prepared=True)

    # returns inserted user id
    def insert_user(self, user):
        query = "insert into users (user_name, user_password, user_role) values (%s, %s, %s)"
        self.usersCursor.execute(query, (user.username, user.password, user.user_role, ))
        self.db.commit()
        return self.usersCursor.lastrowid

    def get_user_by_username(self, username):
        query = "select * from users where user_name = %s"
        self.usersCursor.execute(query, (username, ))
        return self.usersCursor.fetchall()

    def get_user_by_id(self, id):
        query = "select * from users where user_id = %s"
        self.usersCursor.execute(query, (id,))
        return self.usersCursor.fetchall()

    def is_user_exists(self, username):
        query = "select count(*) from users where user_name = %s"
        self.usersCursor.execute(query, (username, ))
        result = self.usersCursor.fetchall()[0]
        if result[0] > 0:
            return True
        else:
            return False

    def get_user_password(self, username):
        query = "select user_password from users where user_name = %s"
        self.usersCursor.execute(query, (username, ))
        return self.usersCursor.fetchall()[0][0]

    def delete_user(self, username):
        query = "delete from users where user_name = %s"
        self.usersCursor.execute(query, (username, ))
        self.db.commit()
