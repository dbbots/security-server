from unittest import TestCase

from config import DATABASE, DB_HOST, DB_USER
from credentials_storage import CredentialsStorage
from user import User
from os import getenv
from security import verify_password, hash_password

test_pass = "test_pass"


class CredentialsStorageTest(TestCase):
    def setUp(self):
        config = {
            "DATABASE": DATABASE,
            "DB_HOST": DB_HOST,
            "DB_USER": DB_USER,
            "DB_PASS": getenv("DB_PASS")
        }
        self.credentials_storage = CredentialsStorage(config)
        self.test_user = User("test_user", test_pass, "trader")
        self.test_user.password = hash_password(self.test_user.password)
        self.user_id = self.credentials_storage.insert_user(self.test_user)

    def test_insert_user(self):
        self.assertIsNotNone(self.user_id, msg="User_id is expected but not presented after insert")
        result = self.credentials_storage.is_user_exists(self.test_user.username)
        self.assertTrue(result, msg="Login from database should be the same")

    def test_verify_user(self):
        hash_pass = self.credentials_storage.get_user_password(self.test_user.username)
        result = verify_password(test_pass, hash_pass)
        self.assertTrue(result, msg="Passwords are not equals by some reason")

    def tearDown(self):
        self.credentials_storage.delete_user(self.test_user.username)
