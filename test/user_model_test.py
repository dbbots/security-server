from unittest import TestCase


from user import User
from user_role import UserRole
from security_flask import storage
from os import getenv


class TestUserModel(TestCase):
    def setUp(self):
        self.secret_key = getenv("SECRET_KEY")

    def test_encode_auth_token(self):
        user = User(
            username='test',
            password='test',
            user_role=UserRole.TRADER.value
        )
        user_id = storage.insert_user(user)
        auth_token = user.encode_auth_token(user_id, secret_key=self.secret_key)
        self.assertTrue(isinstance(auth_token, bytes))

    def test_decode_auth_token(self):
        user = User(
            username='test',
            password='test',
            user_role=UserRole.TRADER.value
        )
        user_id = storage.insert_user(user)
        auth_token = user.encode_auth_token(user_id, secret_key=self.secret_key)
        self.assertTrue(isinstance(auth_token, bytes))
        self.assertEqual(User.decode_auth_token(auth_token, secret_key=self.secret_key), user_id)