from unittest import TestCase

from security import hash_password, verify_password


class SecurityTest(TestCase):
    def test_password_hashing(self):
        password = "my_pass"
        hashed_pass = hash_password(password)
        self.assertTrue(verify_password(password, hashed_pass), msg="Password and hashed password are not the same")

    def test_wrong_password_hashing(self):
        wrong_pass = "another_pass"
        real_pass = "correct_pass"
        wrong_hashed_pass = hash_password(wrong_pass)
        self.assertFalse(verify_password(real_pass, wrong_hashed_pass), msg="Password and another hashed password "
                                                                            "should not be the same")
