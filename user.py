import jwt
import datetime


class User:
    def __init__(self, username, password, user_role):
        self.username = username
        self.password = password
        self.user_role = user_role

    def encode_auth_token(self, user_id, secret_key):
        """
        Generates the Auth Token
        :return: string
        """
        try:
            payload = {
                'exp': datetime.datetime.utcnow() + datetime.timedelta(days=1, seconds=0),
                'iat': datetime.datetime.utcnow(),
                'sub': user_id
            }
            return jwt.encode(
                payload,
                str(secret_key),
                algorithm='HS256'
            )
        except Exception as e:
            return e

    @staticmethod
    def decode_auth_token(auth_token, secret_key):
        """
        Decodes the auth token
        :param secret_key:
        :param auth_token:
        :return: integer|string
        """
        try:
            payload = jwt.decode(auth_token, str(secret_key))
            return payload['sub']
        except jwt.ExpiredSignatureError:
            return 'Signature expired. Please log in again.'
        except jwt.InvalidTokenError:
            return 'Invalid token. Please log in again.'
