from enum import Enum


class UserRole(Enum):
    TRADER = "trader"
    SENIOR_TRADER = "senior_trader"
