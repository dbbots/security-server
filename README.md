# Security server
Provide access to user credentials, availability to add and verify users.
Using tokens to verify users requests.
## Docker information:
```docker
docker build . -t security-server
docker run -e DB_PASS={DB_PASS} -e SECRET_KEY={SECRET_KEY} -e DB_HOST={DB_HOST} -d -p {config.port}:{config.port}
```

## Environment variables
DB_PASS, DB_HOST, SECRET_KEY

##Database
All settings for database is inside config.py file except password.
You may provide password through the environment variable - **DB_PASS**

##Add users
User additions is possible through add_user script in dbutils folder.

## Tokens
Tokens are generated in JWT form. You have to provide secret_key to algorithm worked properly.
Try with a command:
```python
import os
os.urandom(24)
# b"\xf9'\xe4p(\xa9\x12\x1a!\x94\x8d\x1c\x99l\xc7\xb7e\xc7c\x86\x02MJ\xa0"
```
Set this key as an environment variable 
